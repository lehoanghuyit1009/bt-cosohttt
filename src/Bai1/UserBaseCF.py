import math
Dtrain = [
  [0,1,3,4,5],
  [1,2,0,4,5],
  [1,1,3,5,0],
  [0,1,3,4,1],
  [0,2,3,2,3],
]

print("Dtrain: ")
for i in Dtrain:
    print(i)

rowsLength = len(Dtrain)
colsLength = len(Dtrain[0])
rows = []

for i in range(rowsLength):
    for j in range(colsLength):
        if Dtrain[i][j]==0:
            rows.append([i,j])

for item in rows:
    cur = item[0]
    sumSimu = 0
    sumSimr = 0
    for i in range(colsLength):
        if i != cur:
            sum1 = 0
            sum2 = 0
            sum3 = 0
            for j in range (rowsLength):
                if Dtrain[cur][j] != 0 and Dtrain[i][j] !=0:
                    sum1 += Dtrain[cur][j] * Dtrain[i][j] 
                    sum2 += Dtrain[cur][j] * Dtrain[cur][j]
                    sum3 += Dtrain[i][j] * Dtrain[i][j] 
            cosin = sum1/(math.sqrt(sum2*sum2)*math.sqrt(sum3*sum3))
            sumSimu += abs(cosin)
            sumSimr += cosin*Dtrain[i][j]
    Dtrain[item[0]][item[1]] = sumSimr/sumSimu

print("=================")
print("Data: ")
for i in Dtrain:
    print(i)
