import numpy

numpy.random.seed(3)

def printMatrix(matrix):
    for i in matrix:
        print(i)

def matrixFactorizationImProve(A, K, beta=0.04, landa=0.01, loop=5000):
    W = numpy.random.rand(user, K)
    H = numpy.random.rand(K, item)

    step = 0
    while step < loop:
        for u in range(0, user):
            for i in range(0, item):
                if A[u][i] > 0:
                    r3 = 0
                    for k in range(K):
                        r3 += numpy.dot(W[u][k], H[k][i])
                    eui = A[u][i] - r3
                    for k in range(K):
                        W[u][k] += beta * (2 * eui * H[k][i] - landa * W[u][k])
                        H[k][i] += beta * (2 * eui * W[u][k] - landa * H[k][i])
        step += 1
    return W, H


A = numpy.array([
    [2, 4, 0, 1, 3],
    [4, 2, 1, 0, 2],
    [5, 3, 1, 2, 0],
    [1, 5, 0, 3, 4]
])

user = A.shape[0]
item = A.shape[1]

W, K = matrixFactorizationImProve(A, 3)
Y = numpy.dot(W, K)
C = numpy.array([[round(Y[i][j]) for j in range(item)] for i in range(user)])

print('Matrix A: ')
printMatrix(A)
print("===============")
print('Matrix W: ')
printMatrix(W)
print("===============")
print('Matrix H: ')
printMatrix(K)
print("===============")
print('Matrix A3: ')
printMatrix(C)