import numpy as np

A = np.array([
    [1 / 1, 1 / 2, 3 / 1],
    [2 / 1, 1 / 1, 4 / 1],
    [1 / 3, 1 / 4, 1 / 1]
])

num_rows, num_cols = A.shape

# Step 1
sum_col = A.sum(axis=0)

# Step 2
B = np.zeros(shape=(num_rows, num_cols))

for row in range(0, num_rows):
    for col in range(0, num_cols):
        B[row][col] = A[row][col] / sum_col[col]

# Step 3
sum_row = B.sum(axis=1)
sum_all_row = sum_row.sum(axis=0)

# Step 4
W = np.zeros(num_rows)
for row in range(0, num_rows):
    W[row] = sum_row[row] / sum_all_row

# Step 5 nhan ma tran ban dau voi vecto trong so 
V = A.dot(W)

# Step 6
for row in range(0, num_rows):
    V[row] = V[row] / W[row]

# Step 7
lan_da = V.sum(axis=0) / num_rows

# Step 8 
CI = (lan_da - num_rows) / (num_rows - 1)

# Step 9
RI = np.array([0, 0, 0.52, 0.89, 1.11, 1.25, 1.35, 1.4, 1.45, 1.49, 1.52, 1.54, 1.56, 1.58, 1.59])

CR = CI / RI[num_rows]

print(CR)

print(W)

# 1. nhan ma tran so  sanh ban dau voi vecto trong so -> tong vector trong so
# 2. chia vecto tong trong so cho vecto trong so da duoc xac dinh truoc do => vecto nhat quan 
# 3. tinh gai tri rieng lon nhat, lan_da  = trung binh vectoc nhat quan
# 4. tinh chi so nhat quan CI, CI = (landa -n)/(n-1)
# 5. ti so nha tquan : CR = CI /RI